
RSS Enhanced is designed to be a starting-off point for further RSS
customizations.  Out of the box, it will integrate Tweetmeme, Service Links, and
comments directly into your feed.  With very little additional work, you can
add related posts and taxonomy terms.

Because this module is designed to be a template for customizations, we
recommend that you copy this functionality into your own custom module so
you can be sure that when upgrading your contributed modules, nothing breaks.

Please note that if you are integrating your feed with the service_links module,
you MUST USE the 2.x branch of service_links.
